# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .handlers import HelloHandler
from minty_amqp.consumer import BaseConsumer


class HelloEventsConsumer(BaseConsumer):
    def _register_routing(self):
        self._known_handlers = [HelloHandler(self.cqrs)]

        self.routing_keys = []
        for handler in self._known_handlers:
            self.routing_keys.extend(handler.routing_keys)
